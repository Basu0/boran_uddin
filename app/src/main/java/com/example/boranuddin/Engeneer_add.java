package com.example.boranuddin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;

import static maes.tech.intentanim.CustomIntent.customType;

public class Engeneer_add extends AppCompatActivity {
   EditText name,number,email,componyname,engNote;
    DatabaseReference reference;
    StorageReference storageReference;
    FirebaseDatabase database;
    private ProgressDialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engeneer_add);
        name=(EditText)findViewById(R.id.nameid);
        number=(EditText)findViewById(R.id.numberid);
        email=(EditText)findViewById(R.id.emailid);
        componyname=(EditText)findViewById(R.id.componyid);
        engNote=(EditText)findViewById(R.id.Noteedit);
        database=FirebaseDatabase.getInstance();
        reference=database.getReference("EngineerTable");
        progressBar = new ProgressDialog(this);


    }

    public void EngAdd(){
       String namev=name.getText().toString();
       String  nuberv=number.getText().toString();
       String  emailv=email.getText().toString();
       String  compoyv=componyname.getText().toString();
        String  Engnote=engNote.getText().toString();
        String smsUId=reference.push().getKey();
        progressBar.setTitle("Engineer");
        progressBar.setMessage("Please wait, Loading");
        //progressBar.setCanceledOnTouchOutside(false);
        progressBar.show();
        HashMap<Object,String> hashMap=new HashMap<>();
        hashMap.put("Name",namev);
        hashMap.put("Number",nuberv);
        hashMap.put("Email",emailv);
        hashMap.put("Compony",compoyv);
        hashMap.put("UID",smsUId);
        hashMap.put("EngeneerNote",Engnote);

        reference.child(smsUId).setValue(hashMap);
        Toast.makeText(getApplication(),"add Success",Toast.LENGTH_LONG).show();
        progressBar.dismiss();

    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(Engeneer_add.this,MainActivity.class));
            customType(Engeneer_add.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void submit(View view) {
        String namev=name.getText().toString();
        String  nuberv=number.getText().toString();
        String  emailv=email.getText().toString();
        String  compoyv=componyname.getText().toString();
        android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
                .setMessage("Name: "+namev+"\n"+"Number :"+nuberv+"\n"+"Email :"+emailv+"\n"+"Company :"+compoyv)
                .setTitle("Do you want to Appointment")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        EngAdd();




                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();
    }
}
