package com.example.boranuddin;

public class Stock_Show_class {
    public String productName,product1,product2,product3;
    public Stock_Show_class(){}

    public Stock_Show_class(String productName, String product1, String product2, String product3) {
        this.productName = productName;
        this.product1 = product1;
        this.product2 = product2;
        this.product3 = product3;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProduct1() {
        return product1;
    }

    public void setProduct1(String product1) {
        this.product1 = product1;
    }

    public String getProduct2() {
        return product2;
    }

    public void setProduct2(String product2) {
        this.product2 = product2;
    }

    public String getProduct3() {
        return product3;
    }

    public void setProduct3(String product3) {
        this.product3 = product3;
    }
}
