package com.example.boranuddin;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class Eng_show_listview extends AppCompatActivity {
    ListView listView;
    public List<EngShowClass> inlist;
    private SearchView searchView;
    public String value;
    public Eng_adpatar adptar;
    EngShowClass insdfert;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eng_show_listview);
        listView=(ListView)findViewById(R.id.enlist);
        searchView=(SearchView) findViewById(R.id.searchviewid);
        databaseReference=FirebaseDatabase.getInstance().getReference("EngineerTable");
        inlist =new ArrayList<>();
        adptar  =new Eng_adpatar(Eng_show_listview.this,inlist);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (!TextUtils.isEmpty(s.trim())){
                    searchitam(s);
                }
                else {
                    //onStart();

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!TextUtils.isEmpty(s.trim())){
                    searchitam(s);
                }
                else {
                    //onStart();

                }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        //Query query=databaseReference.orderByChild("ProductItem").equalTo(value);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    EngShowClass inserty=dataSnapshot1.getValue(EngShowClass.class);
                    //String ddd= inserty.getC16mmprice();
                    inlist.add(inserty);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            for (int i = 0; i < listView.getCount(); i++)


                                if (position==i){
                                    insdfert=inlist.get(position);
                                    String ff=insdfert.getNumber();
                                    //Toast.makeText(Eng_show_listview.this, "valu "+ff, Toast.LENGTH_SHORT).show();
                                }
                        }
                    });
                    //Toast.makeText(productList.this, "fff"+ddd, Toast.LENGTH_SHORT).show();
                }

                listView.setAdapter(adptar);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(Eng_show_listview.this,MainActivity.class));
            customType(Eng_show_listview.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void showall(View view) {
        onStart();
    }
    private void searchitam(final String queary) {
        //Query query=databaseReference.orderByChild("ProductItem").equalTo(value);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    EngShowClass inserty=dataSnapshot1.getValue(EngShowClass.class);


                    if (inserty.getName().toUpperCase().contains(queary.toUpperCase())){
                        inlist.add(inserty);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @SuppressLint("ResourceAsColor")
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                for (int i = 0; i < listView.getCount(); i++)


                                    if (position==i){
                                        insdfert=inlist.get(position);

                                        String ff=insdfert.getNumber();
                                        //Toast.makeText(Eng_show_listview.this, "valu "+ff, Toast.LENGTH_SHORT).show();
                                    }
                            }
                        });

                    }
                    listView.setAdapter(adptar);


                }
                //adptar.notifyDataSetChanged();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }

    public void callnumbers(View view)  {


        if (insdfert==null){


            Snackbar.make(view, "Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        else {


            callnumber();

        }


    }

    private void dialContactPhone(final String phoneNumber) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }


    public void callnumber(){
        final String phonenumber=insdfert.getNumber();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("CAll");
        alertDialogBuilder.setIcon(android.R.drawable.ic_menu_call);
        alertDialogBuilder.setMessage("Phone Number:"+phonenumber+"");


        alertDialogBuilder.setPositiveButton("Call Now",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        dialContactPhone(""+phonenumber+"");



                    }
                });

        alertDialogBuilder.setNegativeButton("No Call",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
    public void emailsend(View view){
        if (insdfert==null){


            Snackbar.make(view, "Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        else {


            emailsed();

        }

    }

    public void emailsed(){
        final String emailval=insdfert.getEmail();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Email");
        alertDialogBuilder.setIcon(android.R.drawable.ic_dialog_email);
        alertDialogBuilder.setMessage("Email :"+emailval+"");


        alertDialogBuilder.setPositiveButton("Send Now",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/html");
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{""+emailval});
                        i.putExtra(Intent.EXTRA_SUBJECT, "");
                        i.putExtra(Intent.EXTRA_TEXT, "");
                        try {
                            startActivity(Intent.createChooser(i, "Send Email"));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(getApplicationContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }


                    }
                });

        alertDialogBuilder.setNegativeButton("No Send",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }

    public void deletepostd(View view){
        if (insdfert==null){


            Snackbar.make(view, "Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        else {

            deletepost();

        }

    }
public void deletepost(){
    final String uid=insdfert.getUID();
    String name=insdfert.getName();
    String Compony=insdfert.getCompony();
    String Phone=insdfert.getNumber();
    android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
            .setMessage(" Name : " +name+" \n phone : " +Phone+" \n Compony : "+Compony+"")
            .setTitle("Do you want to Delete")

            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                // do something when the button is clicked
                public void onClick(DialogInterface arg0, int arg1) {

                    String sid=insdfert.getUID();

                    //Toast.makeText(getApplication(),"Hello"+insdfert.getNeedbl()+"/n"+insdfert.getPhone()+"",Toast.LENGTH_LONG).show();

                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                    Query applesQuery = ref.child("EngineerTable").orderByChild("UID").equalTo(sid);

                    applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                appleSnapshot.getRef().removeValue();
                                inlist.clear();
                                //Showdata();

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });



                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {

                // do something when the button is clicked
                public void onClick(DialogInterface arg0, int arg1) {
                }
            })
            .show();

}

}
