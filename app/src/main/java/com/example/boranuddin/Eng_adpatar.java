package com.example.boranuddin;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class Eng_adpatar extends ArrayAdapter<EngShowClass> {

    private Activity context;
    private List<EngShowClass> productShowClassList;
    public List<EngShowClass> arraylist;

    public Eng_adpatar(Activity context, List<EngShowClass> productShowClassList) {
        super(context, R.layout.eng_simple_layout, productShowClassList);
        this.context = context;
        this.productShowClassList = productShowClassList;

    }


    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        View view=layoutInflater.inflate(R.layout.eng_simple_layout,null,true);

        EngShowClass productshow=productShowClassList.get(position);

        TextView Name=(TextView) view.findViewById(R.id.namedp);
        TextView Email=(TextView) view.findViewById(R.id.emailadd);
        TextView Phone=(TextView) view.findViewById(R.id.phonenu);
        TextView Compony=(TextView) view.findViewById(R.id.componyn);
        TextView Engnoteshow=(TextView) view.findViewById(R.id.engnote);


        Name.setText(""+productshow.getName());
        Email.setText(""+productshow.getEmail());
        Phone.setText(""+productshow.getNumber());
        Compony.setText(""+productshow.getCompony());
        Engnoteshow.setText(""+productshow.getEngeneerNote());



        return view;
    }



}
