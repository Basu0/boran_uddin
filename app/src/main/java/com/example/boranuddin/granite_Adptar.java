package com.example.boranuddin;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class granite_Adptar extends ArrayAdapter<Granite_Show_class> {
    private Activity context;
    private List<Granite_Show_class> GraniteShowClass;

    public granite_Adptar(Activity context, List<Granite_Show_class> graniteShowClass) {
        super(context, R.layout.granite_simple_layout, graniteShowClass);
        this.context = context;
        GraniteShowClass = graniteShowClass;


    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        View view=layoutInflater.inflate(R.layout.granite_simple_layout,null,true);

        Granite_Show_class productshow=GraniteShowClass.get(position);

        TextView proName=(TextView) view.findViewById(R.id.sampletext2);
        TextView r116tk=(TextView) view.findViewById(R.id.r12tk);
        TextView r218tk=(TextView) view.findViewById(R.id.r15tk);
        TextView r316tk=(TextView) view.findViewById(R.id.r18tk);
        TextView c12tk=(TextView)view.findViewById(R.id.c12tk);
        TextView c15tk=(TextView)view.findViewById(R.id.c15tk);
        TextView c18tk=(TextView)view.findViewById(R.id.c18tk);
        TextView Quantity=(TextView)view.findViewById(R.id.Quantity);
        TextView Country=(TextView)view.findViewById(R.id.Country);

        TextView CompanyName=(TextView) view.findViewById(R.id.companeShow2);


        proName.setText(""+productshow.getProductName());
        r116tk.setText(""+productshow.getR12mmprice()+" Tk");
        r218tk.setText(""+productshow.getR15mprice()+" Tk");
        r316tk.setText(""+productshow.getR18mmprice()+" Tk");
        c12tk.setText(""+productshow.getC12mmprice()+" Tk");
        c15tk.setText(""+productshow.getC15mmprice()+" Tk");
        c18tk.setText(""+productshow.getC18mmprice()+" Tk");
        CompanyName.setText("Company :" +productshow.getCompanyName());
        Quantity.setText(""+productshow.getGraniteQuentat()+"");
        Country.setText(""+productshow.getGraniteContry()+"");



        return view;
    }



}
