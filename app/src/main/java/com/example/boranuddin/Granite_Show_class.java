package com.example.boranuddin;

public class Granite_Show_class {
    private String ProductItem,productName,R12mmprice,R15mprice,R18mmprice,CompanyName,C12mmprice,C15mmprice,C18mmprice,GraniteQuentat,GraniteContry;

    public Granite_Show_class(){}

    public Granite_Show_class(String c12mmprice, String c15mmprice, String c18mmprice) {
        C12mmprice = c12mmprice;
        C15mmprice = c15mmprice;
        C18mmprice = c18mmprice;
    }

    public String getGraniteQuentat() {
        return GraniteQuentat;
    }

    public void setGraniteQuentat(String graniteQuentat) {
        GraniteQuentat = graniteQuentat;
    }

    public String getGraniteContry() {
        return GraniteContry;
    }

    public Granite_Show_class(String graniteQuentat, String graniteContry) {
        GraniteQuentat = graniteQuentat;
        GraniteContry = graniteContry;
    }

    public void setGraniteContry(String graniteContry) {
        GraniteContry = graniteContry;
    }

    public String getC12mmprice() {
        return C12mmprice;
    }

    public void setC12mmprice(String c12mmprice) {
        C12mmprice = c12mmprice;
    }

    public String getC15mmprice() {
        return C15mmprice;
    }

    public void setC15mmprice(String c15mmprice) {
        C15mmprice = c15mmprice;
    }

    public String getC18mmprice() {
        return C18mmprice;
    }

    public void setC18mmprice(String c18mmprice) {
        C18mmprice = c18mmprice;
    }

    public Granite_Show_class(String productItem, String productName, String r12mmprice, String r15mprice, String r18mmprice, String companyName) {
        this.ProductItem = productItem;
        this.productName = productName;
        this.R12mmprice = r12mmprice;
        this.R15mprice = r15mprice;
        this.R18mmprice = r18mmprice;
        this.CompanyName = companyName;
    }

    public String getProductItem() {
        return ProductItem;
    }

    public void setProductItem(String productItem) {
        ProductItem = productItem;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getR12mmprice() {
        return R12mmprice;
    }

    public void setR12mmprice(String r12mmprice) {
        R12mmprice = r12mmprice;
    }

    public String getR15mprice() {
        return R15mprice;
    }

    public void setR15mprice(String r15mprice) {
        R15mprice = r15mprice;
    }

    public String getR18mmprice() {
        return R18mmprice;
    }

    public void setR18mmprice(String r18mmprice) {
        R18mmprice = r18mmprice;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }
}
