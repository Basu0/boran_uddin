package com.example.boranuddin;

public class productShowClass {
    private String ProductItem,productName,R16mmprice,R18mmprice,C16mmprice,C18mmprice,CompanyName,marbrQuentaty,marbrCountry;

    public productShowClass(){


    }

    public productShowClass(String companyName) {
        CompanyName = companyName;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getMarbrQuentaty() {
        return marbrQuentaty;
    }

    public void setMarbrQuentaty(String marbrQuentaty) {
        this.marbrQuentaty = marbrQuentaty;
    }

    public String getMarbrCountry() {
        return marbrCountry;
    }

    public void setMarbrCountry(String marbrCountry) {
        this.marbrCountry = marbrCountry;
    }

    public productShowClass(String marbrQuentaty, String marbrCountry) {
        this.marbrQuentaty = marbrQuentaty;
        this.marbrCountry = marbrCountry;
    }

    public productShowClass(String productItem, String productName, String r16mmprice, String r18mmprice, String c16mmprice, String c18mmprice) {
        this.ProductItem = productItem;
        this.productName = productName;
        this.R16mmprice = r16mmprice;
        this.R18mmprice = r18mmprice;
        this.C16mmprice = c16mmprice;
        this.C18mmprice = c18mmprice;
    }

    public String getProductItem() {
        return ProductItem;
    }

    public void setProductItem(String productItem) {
        ProductItem = productItem;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getR16mmprice() {
        return R16mmprice;
    }

    public void setR16mmprice(String r16mmprice) {
        R16mmprice = r16mmprice;
    }

    public String getR18mmprice() {
        return R18mmprice;
    }

    public void setR18mmprice(String r18mmprice) {
        R18mmprice = r18mmprice;
    }

    public String getC16mmprice() {
        return C16mmprice;
    }

    public void setC16mmprice(String c16mmprice) {
        C16mmprice = c16mmprice;
    }

    public String getC18mmprice() {
        return C18mmprice;
    }

    public void setC18mmprice(String c18mmprice) {
        C18mmprice = c18mmprice;
    }
}
