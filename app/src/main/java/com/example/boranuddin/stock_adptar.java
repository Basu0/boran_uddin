package com.example.boranuddin;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class stock_adptar extends ArrayAdapter<Stock_Show_class> {

    private Activity context;
    private List<Stock_Show_class> productShowClassList;
    public List<Stock_Show_class> arraylist;

    public stock_adptar(Activity context, List<Stock_Show_class> productShowClassList) {
        super(context, R.layout.stock_sample_layout, productShowClassList);
        this.context = context;
        this.productShowClassList = productShowClassList;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        View view=layoutInflater.inflate(R.layout.stock_sample_layout,null,true);

        Stock_Show_class productshow=productShowClassList.get(position);

        TextView proName=(TextView) view.findViewById(R.id.sampletextStock);
        TextView product8_12=(TextView) view.findViewById(R.id.product8_12);
        TextView product12_24=(TextView) view.findViewById(R.id.product12_24);
        TextView product12_12=(TextView) view.findViewById(R.id.product12_12);



        proName.setText(""+productshow.getProductName());
        product8_12.setText(""+productshow.getProduct1()+" PCS");
        product12_24.setText(""+productshow.getProduct2()+" PCS");
        product12_12.setText(""+productshow.getProduct3()+" PCS");



        return view;
    }



}
