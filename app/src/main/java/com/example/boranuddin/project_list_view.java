package com.example.boranuddin;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class project_list_view extends AppCompatActivity {
    ListView listView;
    public List<Project_Show_Class> inlist;
    private SearchView searchView;
    public String value;
    public Project_addptar adptar;
    Project_Show_Class insdfert;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list_view);

        listView=(ListView)findViewById(R.id.enlist);
        searchView=(SearchView) findViewById(R.id.searchviewid);
        databaseReference=FirebaseDatabase.getInstance().getReference("Project_AddTable");
        inlist =new ArrayList<>();
        adptar  =new Project_addptar(project_list_view.this,inlist);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (!TextUtils.isEmpty(s.trim())){
                    searchitam(s);
                }
                else {
                    //onStart();

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!TextUtils.isEmpty(s.trim())){
                    searchitam(s);
                }
                else {
                    //onStart();

                }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        //Query query=databaseReference.orderByChild("ProductItem").equalTo(value);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    Project_Show_Class inserty=dataSnapshot1.getValue(Project_Show_Class.class);
                    //String ddd= inserty.getC16mmprice();
                    inlist.add(inserty);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            for (int i = 0; i < listView.getCount(); i++)


                                if (position==i){
                                    insdfert=inlist.get(position);
                                    String ff=insdfert.getNumberr();
                                    //Toast.makeText(Eng_show_listview.this, "valu "+ff, Toast.LENGTH_SHORT).show();
                                }
                        }
                    });
                    //Toast.makeText(productList.this, "fff"+ddd, Toast.LENGTH_SHORT).show();
                }

                listView.setAdapter(adptar);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }



    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(project_list_view.this,MainActivity.class));
            customType(project_list_view.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void showall(View view) {
        onStart();
    }

    private void searchitam(final String queary) {
        //Query query=databaseReference.orderByChild("ProductItem").equalTo(value);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    Project_Show_Class inserty=dataSnapshot1.getValue(Project_Show_Class.class);


                    if (inserty.getCompany_name().toUpperCase().contains(queary.toUpperCase()) || inserty.getEnginee_name().toUpperCase().contains(queary.toUpperCase())
                            || inserty.getNumberr().toUpperCase().contains(queary.toUpperCase())
                    ){
                        inlist.add(inserty);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @SuppressLint("ResourceAsColor")
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                for (int i = 0; i < listView.getCount(); i++)


                                    if (position==i){
                                        insdfert=inlist.get(position);


                                       // Toast.makeText(project_list_view.this, "valu "+insdfert.getCompany_name(), Toast.LENGTH_SHORT).show();
                                    }
                            }
                        });

                    }
                    listView.setAdapter(adptar);


                }
                //adptar.notifyDataSetChanged();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }

    public void callnumbers(View view)  {


        if (insdfert==null){


            Snackbar.make(view, "Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        else {


            callnumber();

        }


    }

    private void dialContactPhone(final String phoneNumber) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }


    public void callnumber(){
        final String phonenumber=insdfert.getNumberr();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("CAll");
        alertDialogBuilder.setIcon(android.R.drawable.ic_menu_call);
        alertDialogBuilder.setMessage("Phone Number:"+phonenumber+"");


        alertDialogBuilder.setPositiveButton("Call Now",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        dialContactPhone(""+phonenumber+"");



                    }
                });

        alertDialogBuilder.setNegativeButton("No Call",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void deletepostd(View view){
        if (insdfert==null){


            Snackbar.make(view, "Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        else {

            deletepost();

        }

    }
    public void deletepost(){
        final String uid=insdfert.getUID();
        String name=insdfert.getEnginee_name();
        String Company=insdfert.getCompany_name();
        String Phone=insdfert.getNumberr();
        android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
                .setMessage(" Name : " +name+" \n phone : " +Phone+" \n Compony : "+Company+"")
                .setTitle("Do you want to Delete")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        String sid=insdfert.getUID();

                        //Toast.makeText(getApplication(),"Hello"+insdfert.getNeedbl()+"/n"+insdfert.getPhone()+"",Toast.LENGTH_LONG).show();

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                        Query applesQuery = ref.child("Project_AddTable").orderByChild("UID").equalTo(sid);

                        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                    appleSnapshot.getRef().removeValue();
                                    inlist.clear();
                                    //Showdata();

                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });



                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }
}
