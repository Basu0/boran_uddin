package com.example.boranuddin;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.util.Calendar;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static maes.tech.intentanim.CustomIntent.customType;

public class Project_add extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private static final int REQUEST_LOCATION = 1;
    Button datepic;
    EditText compName,EngNamer,phonen,addressn,otherinfo,noten;
    DatabaseReference reference;
    StorageReference storageReference;
    FirebaseDatabase database;
    private ProgressDialog progressBar;
    LocationManager locationManager;
    String lattitude,longitude;
    Geocoder geocoder;
    List<Address> addressList;
    String area;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_add);
        datepic=(Button)findViewById(R.id.datepick);
        compName=(EditText)findViewById(R.id.Cname);
        EngNamer=(EditText)findViewById(R.id.EngName);
        phonen=(EditText)findViewById(R.id.phone);
        addressn=(EditText)findViewById(R.id.address);
        otherinfo=(EditText)findViewById(R.id.otherinfo);
        noten=(EditText)findViewById(R.id.Note);

        database=FirebaseDatabase.getInstance();
        reference=database.getReference("Project_AddTable");
        progressBar = new ProgressDialog(this);



        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
        else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }

        datepic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dateshow();


            }
        });



    }
    public void dateshow(){

        DatePickerDialog datePickerDialog=new DatePickerDialog(
                this,this,
                java.util.Calendar.getInstance().get(Calendar.YEAR),
                java.util.Calendar.getInstance().get(Calendar.MONTH),
                java.util.Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        );
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String daty= +dayOfMonth+"/"+(month+1)+"/"+year;
        datepic.setText(daty);

    }

    public void EngAdd(){
        String ComName=compName.getText().toString();
        String  EngName=EngNamer.getText().toString();
        String  Phneb=phonen.getText().toString();
        String  Add=addressn.getText().toString();
        String  notef=noten.getText().toString();
        String  otinfo=otherinfo.getText().toString();
        String datef=datepic.getText().toString();
        String smsUId=reference.push().getKey();
        progressBar.setTitle("Engineer");
        progressBar.setMessage("Please wait, Loading");
        //progressBar.setCanceledOnTouchOutside(false);
        progressBar.show();
        HashMap<Object,String> hashMap=new HashMap<>();
        hashMap.put("Company_name",ComName);
        hashMap.put("Enginee_name",EngName);
        hashMap.put("Numberr",Phneb);
        hashMap.put("Address",Add);
        hashMap.put("Note",notef);
        hashMap.put("Other_info",otinfo);
        hashMap.put("UID",smsUId);
        hashMap.put("Date",datef);

        reference.child(smsUId).setValue(hashMap);
        Toast.makeText(getApplication(),"add Success",Toast.LENGTH_LONG).show();
        progressBar.dismiss();

    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(Project_add.this,MainActivity.class));
            customType(Project_add.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void submitproject(View view) {
        String ComName=compName.getText().toString();
        String  EngName=EngNamer.getText().toString();
        String  Phneb=phonen.getText().toString();
        String  Add=addressn.getText().toString();
        String  notef=noten.getText().toString();
        String  otinfo=otherinfo.getText().toString();

        android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
                .setMessage("Company Name: "+ComName+"\n"+"Eng:Name :"+EngName+"\n"+"Phone :"+Phneb+"\n"+"Address :"+Add+"\n"+"Note :"+notef+"\n"+"Other Info :"+otinfo)
                .setTitle("Do you want to Submit")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        EngAdd();




                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();
    }



    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(Project_add.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (Project_add.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(Project_add.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
                geocoder=new Geocoder(this, Locale.getDefault());
                try {
                    addressList=geocoder.getFromLocation(latti,longi,1);
                    String address=addressList.get(0).getAddressLine(0);
                    area=addressList.get(0).getLocality();
                    String city=addressList.get(0).getAdminArea();
                    String county=addressList.get(0).getCountryName();

                    addressn.setText(address);

                } catch (IOException e) {
                    e.printStackTrace();
                }




            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
                geocoder=new Geocoder(this, Locale.getDefault());
                try {
                    addressList=geocoder.getFromLocation(latti,longi,1);
                    String address=addressList.get(0).getAddressLine(0);
                    String are=addressList.get(0).getLocality();
                    String city=addressList.get(0).getAdminArea();
                    String county=addressList.get(0).getCountryName();

                    addressn.setText(address);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
                geocoder=new Geocoder(this, Locale.getDefault());
                try {
                    addressList=geocoder.getFromLocation(latti,longi,1);
                    String address=addressList.get(0).getAddressLine(0);
                    area=addressList.get(0).getLocality();
                    String city=addressList.get(0).getAdminArea();
                    String county=addressList.get(0).getCountryName();

                    addressn.setText(""+address);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }else{

                Toast.makeText(this,"Unble to Trace your location",Toast.LENGTH_SHORT).show();

            }
        }
    }
}
