package com.example.boranuddin;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Stock_Data_list extends AppCompatActivity {
    ListView listView;
    public List<Stock_Show_class> inlist;
    private SearchView searchView;
    public stock_adptar adptar;
    Stock_Show_class insdfert;
    private DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock__data_list);
        listView=(ListView)findViewById(R.id.pList);
        databaseReference= FirebaseDatabase.getInstance().getReference("StockTable");

        searchView=(SearchView) findViewById(R.id.searchviewid);
        inlist =new ArrayList<>();
        adptar  =new stock_adptar(Stock_Data_list.this,inlist);
    }

    @Override
    protected void onStart() {
        //Query query=databaseReference.orderByChild("ProductItem").equalTo(value);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    Stock_Show_class inserty=dataSnapshot1.getValue(Stock_Show_class.class);
                    //String ddd= inserty.getC16mmprice();
                    inlist.add(inserty);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            for (int i = 0; i < listView.getCount(); i++)


                                if (position==i){
                                    insdfert=inlist.get(position);
                                    //String ff=insdfert.getNumberr();
                                    //Toast.makeText(Eng_show_listview.this, "valu "+ff, Toast.LENGTH_SHORT).show();
                                }
                        }
                    });
                    //Toast.makeText(productList.this, "fff"+ddd, Toast.LENGTH_SHORT).show();
                }

                listView.setAdapter(adptar);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }
}
