package com.example.boranuddin;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Productlist_Edit extends AppCompatActivity {
String PRoductName,R16mm,R18mm,C16mm,C18mm,ProductQuentity,ProductCountry,Company;
EditText ProductEdt,R16Edt,R18Edit,C16Edit,C18Edit,ProductQuentityEdit,ProductCountryEdt,compayEdit;
    FirebaseDatabase firebaseDatabase;
    private ProgressDialog progressBar;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productlist__edit);
        ProductEdt=findViewById(R.id.product_name);
        R16Edt=findViewById(R.id.r16mmedit);
        R18Edit=findViewById(R.id.r18mmedit);
        C16Edit=findViewById(R.id.c16mmedit);
        C18Edit=findViewById(R.id.c18mmedit);
        ProductQuentityEdit=findViewById(R.id.quentityedit);
        ProductCountryEdt=findViewById(R.id.countryedit);
        compayEdit=findViewById(R.id.companyedit);

        progressBar = new ProgressDialog(this);
        databaseReference= FirebaseDatabase.getInstance().getReference("ProductTable");


        Bundle bundle=getIntent().getExtras();
        if (bundle!=null){
            PRoductName=bundle.getString("ProductName");
            R16mm=bundle.getString("R16mm");
            R18mm=bundle.getString("R18mm");
            C16mm=bundle.getString("C16mm");
            C18mm=bundle.getString("C18mm");
            ProductQuentity=bundle.getString("ProductQuentaty");
            ProductCountry=bundle.getString("country");
            Company=bundle.getString("Comapny");

        }
        ProductEdt.setText(PRoductName);
        R16Edt.setText(R16mm);
        R18Edit.setText(R18mm);
        C16Edit.setText(C16mm);
        C18Edit.setText(C18mm);
        ProductQuentityEdit.setText(ProductQuentity);
        ProductCountryEdt.setText(ProductCountry);
        compayEdit.setText(Company);
    }

    public void close(View view) {
        finish();
    }

    public void DeleteData(View view) {
        final String Product_Name=ProductEdt.getText().toString();

        android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
                .setMessage(" Product Name : " +Product_Name+"")
                .setTitle("Do you want to Delete")
                .setIcon(R.drawable.ic_delete_sweep_black_24dp)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                        Query applesQuery = ref.child("ProductTable").orderByChild("productName").equalTo(Product_Name);

                        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                    appleSnapshot.getRef().removeValue();
                                    new SweetAlertDialog(Productlist_Edit.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Oops...")
                                            .setContentText("Delete Data !")
                                            .show();

                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                new SweetAlertDialog(Productlist_Edit.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText(""+databaseError)
                                        .show();
                            }
                        });



                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();
    }

    public void UpdateData(View view) {
        final String prName=ProductEdt.getText().toString();
        final String R16valu=R16Edt.getText().toString();
        final String R18valu=R18Edit.getText().toString();
        final String C16valu=C16Edit.getText().toString();
        final String C18valu=C18Edit.getText().toString();
        final String ProductQuentity=ProductQuentityEdit.getText().toString();
        final String ProductCountry=ProductCountryEdt.getText().toString();
        final String compayEd=compayEdit.getText().toString();
        new AlertDialog.Builder(Productlist_Edit.this)
                .setTitle("Update")
                .setMessage("Product Name : "+prName+"")
                .setIcon(R.drawable.ic_edit_black_24dp)
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
                                progressBar.setTitle("Upate");
                                progressBar.setMessage("Please wait..");
                                progressBar.show();
                                firebaseDatabase = FirebaseDatabase.getInstance();
                                databaseReference = firebaseDatabase.getReference();
                                databaseReference.child("ProductTable").child(prName).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {


                                        dataSnapshot.getRef().child("C16mmprice").setValue(C16valu);
                                        dataSnapshot.getRef().child("C18mmprice").setValue(C18valu);
                                        dataSnapshot.getRef().child("R16mmprice").setValue(R16valu);
                                        dataSnapshot.getRef().child("R18mmprice").setValue(R18valu);
                                        dataSnapshot.getRef().child("marbrCountry").setValue(ProductCountry);
                                        dataSnapshot.getRef().child("marbrQuentaty").setValue(ProductQuentity);
                                        dataSnapshot.getRef().child("CompanyName").setValue(compayEd);
                                        progressBar.dismiss();

                                        new SweetAlertDialog(Productlist_Edit.this, SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Update Success")
                                                .setContentText("Update Data !")
                                                .show();
                                        //Toast.makeText(getApplication(),"Update Success",Toast.LENGTH_LONG).show();

                                        //inlist.clear();


                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Toast.makeText(getApplication(),"Error"+databaseError,Toast.LENGTH_LONG).show();
                                        progressBar.dismiss();
                                    }
                                });

                                dialog.cancel();
                            }
                        })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                }).show();

    }
}
