package com.example.boranuddin;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class MainActivity extends AppCompatActivity {
  LinearLayout mainLayout,homelayout,toolbarlayout,layoutSarchbarStock;
    CoordinatorLayout stocklayot;
    FloatingActionButton fab;
    SwipeRefreshLayout swipeRefresh;

    ListView listView;
    public List<Stock_Show_class> inlist;
    private SearchView searchView;
    public stock_adptar adptar;
    Stock_Show_class insdfert;
    private DatabaseReference databaseReference;
    FirebaseDatabase firebaseDatabase;
    private ProgressDialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        mainLayout=findViewById(R.id.layoutmain);
        homelayout=findViewById(R.id.homelayout);
        toolbarlayout=findViewById(R.id.toobarlayout);
        stocklayot=findViewById(R.id.stocklayout);
        layoutSarchbarStock=findViewById(R.id.serchbarStock);
        swipeRefresh=findViewById(R.id.swipe_refresh);
        searchView=(SearchView) findViewById(R.id.searchviewid);
        progressBar = new ProgressDialog(this);

        databaseReference= FirebaseDatabase.getInstance().getReference("StockTable");

        listView=(ListView)findViewById(R.id.pListAc);
        searchView=(SearchView) findViewById(R.id.searchviewid);
        inlist =new ArrayList<>();
        adptar  =new stock_adptar(MainActivity.this,inlist);
        StockShow();

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                StockShow();
            }
        });

        fab=findViewById(R.id.add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Stock_activity.class));
                customType(MainActivity.this,"up-to-bottom");
                //finish();
            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.actionmain:
                        mainLayout.setVisibility(View.VISIBLE);
                        toolbarlayout.setVisibility(View.VISIBLE);
                        homelayout.setVisibility(View.GONE);
                        stocklayot.setVisibility(View.GONE);
                        layoutSarchbarStock.setVisibility(View.GONE);
                        break;

                }
                switch (menuItem.getItemId()) {
                    case R.id.action_home:
                        mainLayout.setVisibility(View.GONE);
                        homelayout.setVisibility(View.VISIBLE);
                        toolbarlayout.setVisibility(View.VISIBLE);
                        stocklayot.setVisibility(View.GONE);
                        layoutSarchbarStock.setVisibility(View.GONE);
                        break;


                }
                switch (menuItem.getItemId()) {
                    case R.id.action_stock:
                        mainLayout.setVisibility(View.GONE);
                        homelayout.setVisibility(View.GONE);
                        toolbarlayout.setVisibility(View.GONE);
                        stocklayot.setVisibility(View.VISIBLE);

                        layoutSarchbarStock.setVisibility(View.VISIBLE);
                        //startActivity(new Intent(MainActivity.this,Stock_Data_list.class));
                        //customType(MainActivity.this,"up-to-bottom");


                        break;


                }
                return true;
            }
        });



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (!TextUtils.isEmpty(s.trim())){
                    searchitam(s);
                }
                else {
                    //onStart();

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!TextUtils.isEmpty(s.trim())){
                    searchitam(s);
                }
                else {
                    //onStart();

                }
                return false;
            }
        });


    }

    public void addproduct(View view) {
        startActivity(new Intent(MainActivity.this,product_add.class));
        customType(MainActivity.this,"up-to-bottom");
        finish();
    }

    public void itamshow(View view) {
        startActivity(new Intent(MainActivity.this,item_button_click.class));
        customType(MainActivity.this,"up-to-bottom");
        finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    public void marbel(View view) {
        Intent intent=new Intent(MainActivity.this,productList.class);
        intent.putExtra("valu","Marble");
        startActivity(intent);
        finish();
    }
    public void granite(View view) {
        Intent intent=new Intent(MainActivity.this,grinateProduct_list.class);
        intent.putExtra("valu","Granite");
        startActivity(intent);
        finish();
    }

    public void cutpis(View view) {
        Intent intent=new Intent(MainActivity.this,productList.class);
        intent.putExtra("valu","Cutpis");
        startActivity(intent);
        finish();
    }
    public void cutpalicstom(View view) {
        Intent intent=new Intent(MainActivity.this,productList.class);
        intent.putExtra("valu","Pebble Stone");
        startActivity(intent);
        finish();
    }

    public void khabcha(View view) {
        Intent intent=new Intent(MainActivity.this,productList.class);
        intent.putExtra("valu","Khabcha");
        startActivity(intent);
        finish();
    }

    public void design(View view) {
        Intent intent=new Intent(MainActivity.this,productList.class);
        intent.putExtra("valu","Design");
        startActivity(intent);
        finish();
    }

    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        //close();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }

    public void engenarradd(View view) {
        startActivity(new Intent(MainActivity.this,Engeneer_add.class));
        customType(MainActivity.this,"up-to-bottom");
        finish();
    }

    public void engShow(View view) {
        startActivity(new Intent(MainActivity.this,Eng_show_listview.class));
        customType(MainActivity.this,"up-to-bottom");
        finish();
    }

    public void addproject(View view) {
        startActivity(new Intent(MainActivity.this,Project_add.class));
        customType(MainActivity.this,"up-to-bottom");
        finish();
    }

    public void projectview(View view) {
        startActivity(new Intent(MainActivity.this,project_list_view.class));
        customType(MainActivity.this,"up-to-bottom");
        finish();
    }

    public void StockShow() {

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    swipeRefresh.setRefreshing(false);
                    final Stock_Show_class inserty=dataSnapshot1.getValue(Stock_Show_class.class);
                    inlist.add(inserty);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            for (int i = 0; i < listView.getCount(); i++)


                                if (position==i){
                                    insdfert=inlist.get(position);

                                    //startActivity(intent);
                                    //Toast.makeText(MainActivity.this, "fff"+insdfert.getProductName()+"", Toast.LENGTH_SHORT).show();
                                    //Toast.makeText(MainActivity.this, "valu "+ddd, Toast.LENGTH_SHORT).show();
                                }
                            final String ProductName=insdfert.getProductName();
                            final String P8x12=insdfert.getProduct1();
                            final String P12x24=insdfert.getProduct2();
                            final String P12x12=insdfert.getProduct3();

                            Intent intent=new Intent(MainActivity.this,Stock_Edit.class);
                            intent.putExtra("ProductName",ProductName);
                            intent.putExtra("P8x12",P8x12);
                            intent.putExtra("P12x24",P12x24);
                            intent.putExtra("P12x12",P12x12);
                            startActivity(intent);


                        }
                    });


                    /**/

                }

                listView.setAdapter(adptar);
                Collections.reverse((List<?>) inlist);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(MainActivity.this, "databaseError"+databaseError, Toast.LENGTH_SHORT).show();
                progressBar.dismiss();
            }
        });

    }

    private void searchitam(final String queary) {
        //Query query=databaseReference.orderByChild("ProductItem").equalTo(value);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                    Stock_Show_class inserty=dataSnapshot1.getValue(Stock_Show_class.class);


                    if (inserty.getProductName().toUpperCase().contains(queary.toUpperCase()) || inserty.getProduct1().toUpperCase().contains(queary.toUpperCase())
                            || inserty.getProduct2().toUpperCase().contains(queary.toUpperCase())
                    ){
                        inlist.add(inserty);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @SuppressLint("ResourceAsColor")
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                for (int i = 0; i < listView.getCount(); i++)


                                    if (position==i){
                                        insdfert=inlist.get(position);


                                        //Toast.makeText(Eng_show_listview.this, "valu "+ff, Toast.LENGTH_SHORT).show();
                                    }
                            }
                        });

                    }
                    listView.setAdapter(adptar);


                }
                //adptar.notifyDataSetChanged();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }

    }
