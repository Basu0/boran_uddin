package com.example.boranuddin;

public class Project_Show_Class {
    String Company_name,Enginee_name,Numberr,Address,Note,Other_info,UID,Date;
    public Project_Show_Class(){}

    public Project_Show_Class(String Date,String company_name, String Enginee_name, String Numberr, String address, String note, String other_info, String UID) {
        this.Company_name = company_name;
        this.Enginee_name = Enginee_name;
        this.Numberr = Numberr;
        this.Address = address;
        this.Note = note;
        this.Other_info = other_info;
        this.UID = UID;
        this.Date = Date;
    }

    public String getCompany_name() {
        return Company_name;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public void setCompany_name(String company_name) {
        Company_name = company_name;
    }

    public String getEnginee_name() {
        return Enginee_name;
    }

    public void setEnginee_name(String enginee_name) {
        Enginee_name = enginee_name;
    }

    public String getNumberr() {
        return Numberr;
    }

    public void setNumberr(String numberr) {
        Numberr = numberr;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getOther_info() {
        return Other_info;
    }

    public void setOther_info(String other_info) {
        Other_info = other_info;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }
}
