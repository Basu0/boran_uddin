package com.example.boranuddin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static maes.tech.intentanim.CustomIntent.customType;

public class productList extends AppCompatActivity {
    ListView listView;
    public List<productShowClass> inlist;
    private SearchView searchView;
   public String value;
    public product_adptar adptar;
    productShowClass insdfert;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        listView=(ListView)findViewById(R.id.pList);
        searchView=(SearchView) findViewById(R.id.searchviewid);
        databaseReference=FirebaseDatabase.getInstance().getReference("ProductTable");
        inlist =new ArrayList<>();
        adptar  =new product_adptar(productList.this,inlist);

        Bundle bundle=getIntent().getExtras();
        if (bundle!=null){
             value=bundle.getString("valu");
            //Toast.makeText(productList.this, "man "+value, Toast.LENGTH_SHORT).show();
        }


     searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String s) {
          if (!TextUtils.isEmpty(s.trim())){
              searchitam(s);
          }
          else {
              //onStart();

          }
          return false;
      }

      @Override
      public boolean onQueryTextChange(String s) {
          if (!TextUtils.isEmpty(s.trim())){
            searchitam(s);
          }
          else {
              //onStart();

          }
          return false;
      }
  });



}

    private void searchitam(final String queary) {
        Query query=databaseReference.orderByChild("ProductItem").equalTo(value);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                 productShowClass inserty=dataSnapshot1.getValue(productShowClass.class);


                    if (inserty.getProductName().toUpperCase().contains(queary.toUpperCase())){
                        inlist.add(inserty);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @SuppressLint("ResourceAsColor")
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                for (int i = 0; i < listView.getCount(); i++)


                                    if (position==i){
                                        insdfert=inlist.get(position);
                                        //Toast.makeText(productList.this, "valu "+ff, Toast.LENGTH_SHORT).show();
                                    }

                            }
                        });

                    }
                    listView.setAdapter(adptar);


                }
                //adptar.notifyDataSetChanged();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }


    @Override
    protected void onStart() {
        Query query=databaseReference.orderByChild("ProductItem").equalTo(value);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                inlist.clear();
                for (DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){

                    productShowClass inserty=dataSnapshot1.getValue(productShowClass.class);
                   String ddd= inserty.getC16mmprice();
                    inlist.add(inserty);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            for (int i = 0; i < listView.getCount(); i++)


                                if (position==i){
                                    insdfert=inlist.get(position);

                                    //Toast.makeText(productList.this, "valu "+insdfert.getC16mmprice(), Toast.LENGTH_SHORT).show();
                                }
                            final String ProductName=insdfert.getProductName();
                            final String R16mm=insdfert.getR16mmprice();
                            final String R18mm=insdfert.getR18mmprice();
                            final String C16mm=insdfert.getC16mmprice();
                            final String C18mm=insdfert.getC18mmprice();
                            final String ProductQuentaty=insdfert.getMarbrQuentaty();
                            final String country=insdfert.getMarbrCountry();
                            final String Comapny=insdfert.getCompanyName();
                            Intent intent=new Intent(productList.this,Productlist_Edit.class);
                            intent.putExtra("ProductName",ProductName);
                            intent.putExtra("R16mm",R16mm);
                            intent.putExtra("R18mm",R18mm);
                            intent.putExtra("C16mm",C16mm);
                            intent.putExtra("C18mm",C18mm);
                            intent.putExtra("ProductQuentaty",ProductQuentaty);
                            intent.putExtra("country",country);
                            intent.putExtra("Comapny",Comapny);
                            startActivity(intent);
                        }
                    });
                }

                listView.setAdapter(adptar);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        super.onStart();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(productList.this,MainActivity.class));
            customType(productList.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }



    public void deleteproduct(View view){
        if (insdfert==null){


            Snackbar.make(view, "Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        else {

            deletepost();

        }

    }
    public void deletepost(){
        final String Product_Name=insdfert.getProductName();

        android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
                .setMessage(" Product Name : " +Product_Name+"")
                .setTitle("Do you want to Delete")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        String sid=insdfert.getProductName();

                        //Toast.makeText(getApplication(),"Hello"+insdfert.getNeedbl()+"/n"+insdfert.getPhone()+"",Toast.LENGTH_LONG).show();

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                        Query applesQuery = ref.child("ProductTable").orderByChild("productName").equalTo(sid);

                        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                    appleSnapshot.getRef().removeValue();
                                    inlist.clear();
                                    //Showdata();

                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });



                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }

    public void showall(View view) {
        onStart();
    }
}