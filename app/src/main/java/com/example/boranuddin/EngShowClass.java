package com.example.boranuddin;

public class EngShowClass {
    String Name,Number,Email,Compony,UID,EngeneerNote;
    public EngShowClass(){}

    public EngShowClass(String UID,String EngeneerNote) {
        this.UID = UID;
        this.EngeneerNote=EngeneerNote;
    }

    public String getEngeneerNote() {
        return EngeneerNote;
    }

    public void setEngeneerNote(String engeneerNote) {
        EngeneerNote = engeneerNote;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public EngShowClass(String name, String number, String email, String compony) {
        this.Name = name;
        this.Number = number;
        this.Email = email;
        this.Compony = compony;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getCompony() {
        return Compony;
    }

    public void setCompony(String compony) {
        Compony = compony;
    }
}
