package com.example.boranuddin;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class product_adptar extends ArrayAdapter<productShowClass> {

    private Activity context;
    private List<productShowClass> productShowClassList;
    public List<productShowClass> arraylist;

    public product_adptar(Activity context, List<productShowClass> productShowClassList) {
        super(context, R.layout.productlist_sample_layout, productShowClassList);
        this.context = context;
        this.productShowClassList = productShowClassList;

    }


    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        View view=layoutInflater.inflate(R.layout.productlist_sample_layout,null,true);

        productShowClass productshow=productShowClassList.get(position);

        TextView proName=(TextView) view.findViewById(R.id.sampletext);
        TextView r116tk=(TextView) view.findViewById(R.id.r16tk);
        TextView r218tk=(TextView) view.findViewById(R.id.r18tk);
        TextView c116tk=(TextView) view.findViewById(R.id.c16tk);
        TextView c216tk=(TextView) view.findViewById(R.id.c18tk);
        TextView CompanyName=(TextView) view.findViewById(R.id.companeShow);
        TextView ProductQuentity=(TextView) view.findViewById(R.id.quantity);
        TextView Country=(TextView) view.findViewById(R.id.Country);


        proName.setText(""+productshow.getProductName());
        r116tk.setText(""+productshow.getR16mmprice()+" Tk");
        r218tk.setText(""+productshow.getR18mmprice()+" Tk");
        c116tk.setText(""+productshow.getC16mmprice()+" Tk");
        c216tk.setText(""+productshow.getC18mmprice()+" Tk");
        CompanyName.setText("Company :" +productshow.getCompanyName());
        ProductQuentity.setText(""+productshow.getMarbrQuentaty());
        Country.setText(""+productshow.getMarbrCountry());



        return view;
    }



}
