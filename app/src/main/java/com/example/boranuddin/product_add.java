package com.example.boranuddin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class product_add extends AppCompatActivity {
    Spinner Pitem;
    Button submit,submit2;
    EditText pNamem,r16price,r18price,c16price,c18price,Cname,Cname2,marbrQuentatyid,marbrCountryid,graniteQuentatyid,graniteCountryid;
    EditText r12mprice2,r15mprice2,r18gmprice2,gc12mprice,gc15mprice,gc18mprice;
    DatabaseReference reference,reference2;
    StorageReference storageReference;
    FirebaseDatabase database;
    FirebaseAuth auth;
    private ProgressDialog progressBar;
    String ProductItemname;
    LinearLayout layout1,layout2;
    TextView itamnameset;
    @Override
   protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add);
        Pitem=(Spinner)findViewById(R.id.itamlist);
        submit=(Button)findViewById(R.id.submitdata);
        pNamem=(EditText)findViewById(R.id.pName);
        r16price=(EditText)findViewById(R.id.r16mprice);
        r18price=(EditText)findViewById(R.id.r18mprice);
        c16price=(EditText)findViewById(R.id.c16mprice);
        c18price=(EditText)findViewById(R.id.c18mprice);
        Cname=(EditText)findViewById(R.id.compname);
        marbrQuentatyid=(EditText)findViewById(R.id.marbrQuentatyid);
        marbrCountryid=(EditText)findViewById(R.id.marbrCountryid);

        layout1=(LinearLayout)findViewById(R.id.layout1);
        layout2=(LinearLayout)findViewById(R.id.layout2);
        itamnameset=(TextView)findViewById(R.id.itameNameset);


        ///layout2
        r12mprice2=(EditText)findViewById(R.id.r12mprice);
        r15mprice2=(EditText)findViewById(R.id.r15mprice);
        r18gmprice2=(EditText)findViewById(R.id.r18gmprice);
        submit2=(Button)findViewById(R.id.submitdata2);
        Cname2=(EditText)findViewById(R.id.compname2);
        gc12mprice=(EditText)findViewById(R.id.cg12mprice);
        gc15mprice=(EditText)findViewById(R.id.cg15mprice);
        gc18mprice=(EditText)findViewById(R.id.cg18mprice);
        graniteQuentatyid=(EditText)findViewById(R.id.graniteQuentatyid);
        graniteCountryid=(EditText)findViewById(R.id.graniteCountryid);

        database=FirebaseDatabase.getInstance();
        reference=database.getReference("ProductTable");
        reference2=database.getReference("GraniteTable");

        progressBar = new ProgressDialog(this);



       List<String> catagory=new ArrayList<>();
        catagory.add(0,"");
        catagory.add("Marble");
        catagory.add("Granite");
        catagory.add("Cutpis");
        catagory.add("Pebble Stone");
        catagory.add("Khabcha");
        catagory.add("Design");


        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.simpel_speenar_layout,R.id.texview_sampel_speenar,catagory);
        Pitem.setAdapter(adapter);

        Pitem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String Text = parent.getSelectedItem().toString();
                if(Text.equals("Granite")) {
                    layout1.setVisibility(View.GONE);
                    layout2.setVisibility(View.VISIBLE);
                    itamnameset.setText(Text);
                }
                if(Text.equals("Marble")) {
                    layout1.setVisibility(View.VISIBLE);
                    layout2.setVisibility(View.GONE);
                    itamnameset.setText(Text);
                }
                if(Text.equals("Cutpis")) {
                    layout1.setVisibility(View.VISIBLE);
                    layout2.setVisibility(View.GONE);
                    itamnameset.setText(Text);
                }
                if(Text.equals("Pebble Stone")) {
                    layout1.setVisibility(View.VISIBLE);
                    layout2.setVisibility(View.GONE);
                    itamnameset.setText(Text);
                }
                if(Text.equals("Khabcha")) {
                    layout1.setVisibility(View.VISIBLE);
                    layout2.setVisibility(View.GONE);
                    itamnameset.setText(Text);
                }
                if(Text.equals("Design")) {
                    layout1.setVisibility(View.VISIBLE);
                    layout2.setVisibility(View.GONE);
                    itamnameset.setText(Text);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    submit2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            graniteproductadd();

        }
    });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ChackInnetwork();

    }


private void ChackInnetwork() {
        boolean wificonneaction;
        boolean mobileconneaction;
        ConnectivityManager connectivityManager=(ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();
        if (networkInfo !=null && networkInfo.isConnected()){
            wificonneaction=networkInfo.getType()==ConnectivityManager.TYPE_WIFI;
            mobileconneaction=networkInfo.getType()==ConnectivityManager.TYPE_MOBILE;

            if (wificonneaction){

                dataadd();

            }
            else if (mobileconneaction){

                dataadd();

            }



        }else {
            progressBar.setTitle("No Internet Connecation ");
            progressBar.setMessage("Please Connect Internet");
            progressBar.setIcon(R.drawable.ic_do_not_disturb_black_24dp);
            //progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();

        }
    }
        public void dataadd(){
            String ProductItem=Pitem.getSelectedItem().toString();
            String productName=pNamem.getText().toString();
            String R16mmprice=r16price.getText().toString();
            String R18mmprice=r18price.getText().toString();
            String C16mmprice=c16price.getText().toString();
            String C18mmprice=c18price.getText().toString();
            String ComName=Cname.getText().toString();
            String marbrQuentaty=marbrQuentatyid.getText().toString();
            String marbrCountry=marbrCountryid.getText().toString();

            if (Pitem.getSelectedItemPosition()==0){

                Toast.makeText(getApplication(),"Select Product catagory ",Toast.LENGTH_LONG).show();
            }

            if (productName.isEmpty()){

                pNamem.setError("Product Name Empty");
                pNamem.requestFocus();
                return;
            }
            if (R16mmprice.isEmpty()){

                r16price.setError("R 16mm  price Empty");
                r16price.requestFocus();
                return;
            }
            if (R18mmprice.isEmpty()){

                r18price.setError("R 18mm price Empty");
                r18price.requestFocus();
                return;
            }
            if (C16mmprice.isEmpty()){

                c16price.setError("C 16mm price Empty");
                c16price.requestFocus();
                return;
            }
            if (C18mmprice.isEmpty()){

                c18price.setError("C 18mm price Empty");
                c18price.requestFocus();
                return;
            }
            else {
            progressBar.setTitle("Product Add");
            progressBar.setMessage("Please wait, while we are Product Add");
            //progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();

            //FirebaseUser user=auth.getCurrentUser();
            //String Uid=user.getUid();
            HashMap<Object,String> hashMap=new HashMap<>();
            hashMap.put("ProductItem",ProductItem);
            hashMap.put("productName",productName);
            hashMap.put("R16mmprice",R16mmprice);
            hashMap.put("R18mmprice",R18mmprice);
            hashMap.put("C16mmprice",C16mmprice);
            hashMap.put("C18mmprice",C18mmprice);
            hashMap.put("CompanyName",ComName);
            hashMap.put("marbrQuentaty",marbrQuentaty);
            hashMap.put("marbrCountry",marbrCountry);

            //hashMap.put("Uid",Uid);
            String da=reference.push().getKey();
            reference.child(productName).setValue(hashMap);
            Toast.makeText(getApplication()," Product add Success",Toast.LENGTH_LONG).show();
            progressBar.dismiss();

            /*pNamem.setText("");
            r16price.setText("");
            r18price.setText("");
            c16price.setText("");
            c18price.setText("");
            Cname.setText("");*/
            }



        }



    });}

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(product_add.this,MainActivity.class));
            customType(product_add.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

public void graniteproductadd(){
    String ProductItem=Pitem.getSelectedItem().toString();
    String productName=pNamem.getText().toString();
    String r12mpricef=r12mprice2.getText().toString();
    String r15mprice2f=r15mprice2.getText().toString();
    String r18gmprice2f=r18gmprice2.getText().toString();
    String ComName=Cname2.getText().toString();
    String cg12mmPrice=gc12mprice.getText().toString();
    String cg15mmPrice=gc15mprice.getText().toString();
    String cg18mmPrice=gc18mprice.getText().toString();
    String GraniteQuentat=graniteQuentatyid.getText().toString();
    String GraniteContry=graniteCountryid.getText().toString();

    if (Pitem.getSelectedItemPosition()==0){

        Toast.makeText(getApplication(),"Select Product catagory ",Toast.LENGTH_LONG).show();
    }

    if (productName.isEmpty()){

        pNamem.setError("Product Name Empty");
        pNamem.requestFocus();
        return;
    }
    if (r12mpricef.isEmpty()){

        r12mprice2.setError("R 12mm price  Empty");
        r12mprice2.requestFocus();
        return;
    }
    if (r15mprice2f.isEmpty()){

        r15mprice2.setError("R 15 mm price Empty");
        r15mprice2.requestFocus();
        return;
    }
    if (r18gmprice2f.isEmpty()){

        r18gmprice2.setError("R 18 mm price Empty");
        r18gmprice2.requestFocus();
        return;
    }
    if (cg12mmPrice.isEmpty()){

        gc12mprice.setError("C 12 mm price Empty");
        gc12mprice.requestFocus();
        return;
    }
    if (cg15mmPrice.isEmpty()){

        gc15mprice.setError("C 15 mm price Empty");
        gc15mprice.requestFocus();
        return;
    }
    if (cg18mmPrice.isEmpty()){

        gc18mprice.setError("C 18 mm price Empty");
        gc18mprice.requestFocus();
        return;
    }
    else {
        progressBar.setTitle("Product Add");
        progressBar.setMessage("Please wait, while we are Product Add");
        //progressBar.setCanceledOnTouchOutside(false);
        progressBar.show();

        //FirebaseUser user=auth.getCurrentUser();
        //String Uid=user.getUid();
        HashMap<Object,String> hashMap=new HashMap<>();
        hashMap.put("ProductItem",ProductItem);
        hashMap.put("productName",productName);
        hashMap.put("R12mmprice",r12mpricef);
        hashMap.put("R15mprice",r15mprice2f);
        hashMap.put("R18mmprice",r18gmprice2f);
        hashMap.put("C12mmprice",cg12mmPrice);
        hashMap.put("C15mmprice",cg15mmPrice);
        hashMap.put("C18mmprice",cg18mmPrice);
        hashMap.put("CompanyName",ComName);
        hashMap.put("GraniteQuentat",GraniteQuentat);
        hashMap.put("GraniteContry",GraniteContry);
        //hashMap.put("Uid",Uid);
        String da=reference2.push().getKey();
        reference2.child(productName).setValue(hashMap);
        Toast.makeText(getApplication()," Product add Success",Toast.LENGTH_LONG).show();
        progressBar.dismiss();
    }


}

}
