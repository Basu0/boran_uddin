package com.example.boranuddin;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Granite_ProductEdit extends AppCompatActivity {
    String PRoductName,R12mm,R15mm,R18mm,C12mm,C15mm,C18mm,ProductQuentity,ProductCountry,Company;
    EditText ProductEdt,R12Edt,R15Edit,R18Edit,C12Edit,C15Edit,C18Edit,ProductQuentityEdit,ProductCountryEdt,compayEdit;
    FirebaseDatabase firebaseDatabase;
    private ProgressDialog progressBar;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_granite__product_edit);
        ProductEdt=findViewById(R.id.product_name);
        R12Edt=findViewById(R.id.r12mmedit);
        R15Edit=findViewById(R.id.r15mmedit);
        R18Edit=findViewById(R.id.r18mmedit);
        C12Edit=findViewById(R.id.c12mmedit);
        C15Edit=findViewById(R.id.c15mmedit);
        C18Edit=findViewById(R.id.c18mmedit);
        ProductQuentityEdit=findViewById(R.id.quentityedit);
        ProductCountryEdt=findViewById(R.id.countryedit);
        compayEdit=findViewById(R.id.companyedit);
        progressBar = new ProgressDialog(this);
        databaseReference= FirebaseDatabase.getInstance().getReference("GraniteTable");
        Bundle bundle=getIntent().getExtras();
        if (bundle!=null){
            PRoductName=bundle.getString("ProductName");
            R12mm=bundle.getString("R12mm");
            R15mm=bundle.getString("R15mm");
            R18mm=bundle.getString("R18mm");
            C12mm=bundle.getString("C12mm");
            C15mm=bundle.getString("C15mm");
            C18mm=bundle.getString("C18mm");
            ProductQuentity=bundle.getString("ProductQuentaty");
            ProductCountry=bundle.getString("country");
            Company=bundle.getString("Comapny");

            ProductEdt.setText(PRoductName);
            R12Edt.setText(R12mm);
            R15Edit.setText(R15mm);
            R18Edit.setText(R18mm);
            C12Edit.setText(C12mm);
            C15Edit.setText(C15mm);
            C18Edit.setText(C18mm);
            ProductQuentityEdit.setText(ProductQuentity);
            ProductCountryEdt.setText(ProductCountry);
            compayEdit.setText(Company);

        }
    }

    public void UpdateData(View view) {

        final String prName=ProductEdt.getText().toString();
        final String R12valu=R12Edt.getText().toString();
        final String R15valu=R15Edit.getText().toString();
        final String R18valu=R18Edit.getText().toString();
        final String C12valu=C12Edit.getText().toString();
        final String C15valu=C15Edit.getText().toString();
        final String C18valu=C18Edit.getText().toString();
        final String ProductQuentity=ProductQuentityEdit.getText().toString();
        final String ProductCountry=ProductCountryEdt.getText().toString();
        final String compayEd=compayEdit.getText().toString();
        new AlertDialog.Builder(Granite_ProductEdit.this)
                .setTitle("Update")
                .setMessage("Product Name : "+prName+"")
                .setIcon(R.drawable.ic_edit_black_24dp)
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
                                progressBar.setTitle("Upate");
                                progressBar.setMessage("Please wait..");
                                progressBar.show();
                                firebaseDatabase = FirebaseDatabase.getInstance();
                                databaseReference = firebaseDatabase.getReference();
                                databaseReference.child("GraniteTable").child(prName).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {


                                        dataSnapshot.getRef().child("R12mmprice").setValue(R12valu);
                                        dataSnapshot.getRef().child("R15mprice").setValue(R15valu);
                                        dataSnapshot.getRef().child("R18mmprice").setValue(R18valu);
                                        dataSnapshot.getRef().child("C12mmprice").setValue(C12valu);
                                        dataSnapshot.getRef().child("C15mmprice").setValue(C15valu);
                                        dataSnapshot.getRef().child("C18mmprice").setValue(C18valu);
                                        dataSnapshot.getRef().child("CompanyName").setValue(compayEd);
                                        dataSnapshot.getRef().child("GraniteContry").setValue(ProductCountry);
                                        dataSnapshot.getRef().child("GraniteQuentat").setValue(ProductQuentity);
                                        progressBar.dismiss();

                                        new SweetAlertDialog(Granite_ProductEdit.this, SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Update Success")
                                                .setContentText("Update Data !")
                                                .show();
                                        //Toast.makeText(getApplication(),"Update Success",Toast.LENGTH_LONG).show();

                                        //inlist.clear();


                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Toast.makeText(getApplication(),"Error"+databaseError,Toast.LENGTH_LONG).show();
                                        progressBar.dismiss();
                                    }
                                });

                                dialog.cancel();
                            }
                        })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                }).show();


    }

    public void DeleteData(View view) {

        final String Product_Name=ProductEdt.getText().toString();

        android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
                .setMessage(" Product Name : " +Product_Name+"")
                .setTitle("Do you want to Delete")
                .setIcon(R.drawable.ic_delete_sweep_black_24dp)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                        Query applesQuery = ref.child("GraniteTable").orderByChild("productName").equalTo(Product_Name);

                        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                    appleSnapshot.getRef().removeValue();
                                    new SweetAlertDialog(Granite_ProductEdit.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Oops...")
                                            .setContentText("Delete Data !")
                                            .show();

                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                new SweetAlertDialog(Granite_ProductEdit.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText(""+databaseError)
                                        .show();
                            }
                        });



                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();
    }

    public void close(View view) {
        finish();
    }
}
