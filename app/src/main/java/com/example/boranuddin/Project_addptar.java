package com.example.boranuddin;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class Project_addptar extends ArrayAdapter<Project_Show_Class> {
    private Activity context;
    private List<Project_Show_Class> productShowClassList;
    public List<Project_Show_Class> arraylist;

    public Project_addptar(Activity context, List<Project_Show_Class> productShowClassList) {
        super(context, R.layout.project_sample_layout, productShowClassList);
        this.context = context;
        this.productShowClassList = productShowClassList;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        View view=layoutInflater.inflate(R.layout.project_sample_layout,null,true);

        Project_Show_Class productshow=productShowClassList.get(position);

        TextView Name=(TextView) view.findViewById(R.id.enginame);
        TextView Address=(TextView) view.findViewById(R.id.addressd);
        TextView Phone=(TextView) view.findViewById(R.id.phonenu);
        TextView Compony=(TextView) view.findViewById(R.id.compname);
        TextView note=(TextView) view.findViewById(R.id.noteid);
        TextView other_info=(TextView) view.findViewById(R.id.otherin);
        TextView date=(TextView) view.findViewById(R.id.datedp);


        Name.setText(""+productshow.getEnginee_name());
        Address.setText(""+productshow.getAddress());
        Phone.setText(""+productshow.getNumberr());
        Compony.setText(""+productshow.getCompany_name());
        note.setText(""+productshow.getNote());
        other_info.setText(""+productshow.getOther_info());
        date.setText(""+productshow.getDate());




        return view;
    }



}

