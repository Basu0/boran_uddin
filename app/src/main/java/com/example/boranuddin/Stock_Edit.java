package com.example.boranuddin;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Stock_Edit extends AppCompatActivity {
    String pName,product1,product2,product3;
    EditText Product_name,Product_Qu8_12,Product_Qu12_12,Product_Qu12_24;
    FirebaseDatabase firebaseDatabase;
    private ProgressDialog progressBar;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock__edit);

        Product_name=findViewById(R.id.product_name);
        Product_Qu8_12=findViewById(R.id.product_Qu1);
        Product_Qu12_12=findViewById(R.id.product_Qu2);
        Product_Qu12_24=findViewById(R.id.product_Qu3);
        databaseReference= FirebaseDatabase.getInstance().getReference("StockTable");
        Bundle bundle=getIntent().getExtras();
        progressBar = new ProgressDialog(this);

        if (bundle!=null){
            pName=bundle.getString("ProductName");
            product1=bundle.getString("P8x12");
            product2=bundle.getString("P12x24");
            product3=bundle.getString("P12x12");
            //Toast.makeText(productList.this, "man "+value, Toast.LENGTH_SHORT).show();
        }
        Product_name.setText(pName);
        Product_Qu8_12.setText(product1);
        Product_Qu12_12.setText(product2);
        Product_Qu12_24.setText(product3);


    }

    public void close(View view) {
        finish();
    }

    public void UpdateData(View view) {
        final String prName=Product_name.getText().toString();
        final String prduct1=Product_Qu8_12.getText().toString();
        final String prduct2=Product_Qu12_12.getText().toString();
        final String prduct3=Product_Qu12_24.getText().toString();
        new AlertDialog.Builder(Stock_Edit.this)
                .setTitle("Update")
                .setMessage("Product Name : "+prName+"\nProduct_Qu8_12 : "+prduct1+"\nProduct_Qu12_12 : "+prduct2+"\nProduct_Qu12_24 : "+prduct3+"")
                .setIcon(R.drawable.ic_edit_black_24dp)
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
                                progressBar.setTitle("Upate");
                                progressBar.setMessage("Please wait..");
                                progressBar.show();
                                firebaseDatabase = FirebaseDatabase.getInstance();
                                databaseReference = firebaseDatabase.getReference();
                                databaseReference.child("StockTable").child(prName).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {


                                        dataSnapshot.getRef().child("product1").setValue(prduct1);
                                        dataSnapshot.getRef().child("product2").setValue(prduct2);
                                        dataSnapshot.getRef().child("product3").setValue(prduct3);
                                        progressBar.dismiss();

                                        new SweetAlertDialog(Stock_Edit.this, SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Update Success")
                                                .setContentText("Update Data !")
                                                .show();
                                        //Toast.makeText(getApplication(),"Update Success",Toast.LENGTH_LONG).show();

                                        //inlist.clear();


                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Toast.makeText(getApplication(),"Error"+databaseError,Toast.LENGTH_LONG).show();
                                        progressBar.dismiss();
                                    }
                                });

                                dialog.cancel();
                            }
                        })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                }).show();



    }

    public void DeleteData(View view) {
        final String prName=Product_name.getText().toString();
        final String prduct1=Product_Qu8_12.getText().toString();
        final String prduct2=Product_Qu12_12.getText().toString();
        final String prduct3=Product_Qu12_24.getText().toString();
        new AlertDialog.Builder(Stock_Edit.this)
                .setTitle("Delete")
                .setMessage("Product Name : "+prName+"\nProduct_Qu8_12 : "+prduct1+"\nProduct_Qu12_12 : "+prduct2+"\nProduct_Qu12_24 : "+prduct3+"")
                .setIcon(R.drawable.ic_delete_sweep_black_24dp)
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                                Query applesQuery = ref.child("StockTable").orderByChild("productName").equalTo(prName);

                                applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                            appleSnapshot.getRef().removeValue();
                                            new SweetAlertDialog(Stock_Edit.this, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText("Oops...")
                                                    .setContentText("Delete Data !")
                                                    .show();
                                               //finish();

                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        new SweetAlertDialog(Stock_Edit.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Oops...")
                                                .setContentText(""+databaseError)
                                                .show();

                                    }
                                });

                                dialog.cancel();
                            }
                        })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                }).show();
    }
}
