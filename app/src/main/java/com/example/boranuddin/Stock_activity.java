package com.example.boranuddin;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Stock_activity extends AppCompatActivity {

    FirebaseDatabase database;
    public ProgressDialog progressBar;
    DatabaseReference reference;
    EditText Product_name,Product_Qu8_12,Product_Qu12_12,Product_Qu12_24;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_activity);
        Product_name=findViewById(R.id.product_name);
        Product_Qu8_12=findViewById(R.id.product_Qu1);
        Product_Qu12_12=findViewById(R.id.product_Qu2);
        Product_Qu12_24=findViewById(R.id.product_Qu3);


        database=FirebaseDatabase.getInstance();
        reference=database.getReference("StockTable");



    }

    public void submitstock(View view) {
        ChackInnetwork();
    }

    private void ChackInnetwork() {
        boolean wificonneaction;
        boolean mobileconneaction;
        ConnectivityManager connectivityManager=(ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();
        if (networkInfo !=null && networkInfo.isConnected()){
            wificonneaction=networkInfo.getType()==ConnectivityManager.TYPE_WIFI;
            mobileconneaction=networkInfo.getType()==ConnectivityManager.TYPE_MOBILE;

            if (wificonneaction){

                dataadd();

            }
            else if (mobileconneaction){

                dataadd();

            }



        }else {
            progressBar.setTitle("No Internet Connecation ");
            progressBar.setMessage("Please Connect Internet");
            progressBar.setIcon(R.drawable.ic_do_not_disturb_black_24dp);
            //progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();

        }
    }

    private void dataadd() {

        String productName=Product_name.getText().toString();
        String ProductQU_8_12=Product_Qu8_12.getText().toString();
        String ProductQU_12_12=Product_Qu12_12.getText().toString();
        String ProductQU_12_24=Product_Qu12_24.getText().toString();

        if (productName.isEmpty()){

            Product_name.setError("Product Name Empty");
            Product_name.requestFocus();
            return;
        }

        else {
            HashMap<Object,String> hashMap=new HashMap<>();
            hashMap.put("productName",productName);
            hashMap.put("product1",ProductQU_8_12);
            hashMap.put("product2",ProductQU_12_12);
            hashMap.put("product3",ProductQU_12_24);
            String da=reference.push().getKey();
            reference.child(productName).setValue(hashMap);
            new SweetAlertDialog(Stock_activity.this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Good Job")
                    .setContentText("Data Add")
                    .show();
        }

    }

    public void close(View view) {
        finish();
    }
}
