package com.example.boranuddin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class item_button_click extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_button_click);
    }

    public void marbel(View view) {
        Intent intent=new Intent(item_button_click.this,productList.class);
        intent.putExtra("valu","Marble");
        startActivity(intent);
        finish();
    }

    public void granite(View view) {
        Intent intent=new Intent(item_button_click.this,grinateProduct_list.class);
        intent.putExtra("valu","Granite");
        startActivity(intent);
        finish();
    }

    public void cutpis(View view) {
        Intent intent=new Intent(item_button_click.this,productList.class);
        intent.putExtra("valu","Cutpis");
        startActivity(intent);
        finish();
    }

    public void cutpalicstom(View view) {
        Intent intent=new Intent(item_button_click.this,productList.class);
        intent.putExtra("valu","Pebble Stone");
        startActivity(intent);
        finish();
    }

    public void khabcha(View view) {
        Intent intent=new Intent(item_button_click.this,productList.class);
        intent.putExtra("valu","Khabcha");
        startActivity(intent);
        finish();
    }

    public void design(View view) {
        Intent intent=new Intent(item_button_click.this,productList.class);
        intent.putExtra("valu","Design");
        startActivity(intent);
        finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(item_button_click.this,MainActivity.class));
            customType(item_button_click.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void showall(View view) {
        onStart();
    }
}
